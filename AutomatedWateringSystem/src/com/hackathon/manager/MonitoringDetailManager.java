package com.hackathon.manager;

import java.util.ArrayList;
import java.util.List;

import com.hackathon.beans.Coordinate;
import com.hackathon.beans.Patch;
import com.hackathon.beans.TankerTripDetails;
import com.hackathon.beans.Truck;
import com.hackathon.dao.MonitoringDetailsDao;
import com.hackathon.dao.MonitoringViewDao;

public class MonitoringDetailManager {
	MonitoringViewDao monitoringViewDao = new MonitoringViewDao();
	MonitoringDetailsDao monitoringDetailsDao = new MonitoringDetailsDao();

	public List<Patch> getPatchesForTrip(int tripId) {

		TankerTripDetails tripDetails = monitoringViewDao
				.getTripDetailByTripID(tripId);

		String patchDetails = tripDetails.getPatchDetails();
		String[] patchIds = patchDetails.split(",");

		List<Patch> patches = new ArrayList<>();
		for (String patchId : patchIds) {
			patchId = patchId.trim();
			Patch patch = new Patch();
			patch.setId(Integer.parseInt(patchId));
			patches.add(patch);
		}
		patches = monitoringDetailsDao.getCoordinates(patches);
		
		for(Patch patch:patches){
			
			String rawCoordinate = patch.getCoordinates();
			String[] latAndLong = rawCoordinate.split(",");
			Coordinate coordinate  =  new Coordinate();
			coordinate.setLat(Double.parseDouble(latAndLong[0]));
			coordinate.setLng(Double.parseDouble(latAndLong[1]));
			patch.setCoordinate(coordinate);
		}
		
		return patches;
	}

	public Coordinate getTruckCurrentPosition(int tankerId) {

		Truck truckDetails = monitoringDetailsDao
				.getTruckDetails(tankerId);
		String rawCoordinate = truckDetails.getCoordinate();
		
		String[] latAndLong = rawCoordinate.split(",");
		Coordinate coordinate  =  new Coordinate();
		coordinate.setLat(Double.parseDouble(latAndLong[0]));
		coordinate.setLng(Double.parseDouble(latAndLong[1]));
		return coordinate;
	}
	
	public List<Coordinate> getRouteCoodinates(int tripId) {
		List<Coordinate> coordinates = new ArrayList<>();
		
		String rawCoordinate = monitoringDetailsDao.getRawTripCoordinates(tripId);
		 
		String[] latLongs = rawCoordinate.split("\\|");
		
		for(String latLomg: latLongs){
			if(latLomg.contains(",")){
			Coordinate coordinate = new Coordinate();
			String[] latAndLong = latLomg.split(",");	
			coordinate.setLat(Double.parseDouble(latAndLong[0]));
			coordinate.setLng(Double.parseDouble(latAndLong[1]));
			coordinates.add(coordinate);
			}
		}
		return coordinates;
	}

	public Patch updatePatchStatus(int patchId, String status) {

		monitoringDetailsDao.updatePatchStatus(patchId,status);
		return null;
	}
}
