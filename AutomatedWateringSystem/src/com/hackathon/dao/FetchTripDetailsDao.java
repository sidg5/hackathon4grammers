package com.hackathon.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.hackathon.beans.Coordinate;
import com.hackathon.beans.WaterDispenseDetails;

public class FetchTripDetailsDao 
{
	private ConnectToDB connectToDB;
	private MonitoringDetailsDao monitoringDetailsDao;

	public FetchTripDetailsDao()
	{
		connectToDB = new ConnectToDB();
		monitoringDetailsDao = new MonitoringDetailsDao();
	}
	
	public int getTripId(int tankerId)
	{
		int tripID = 0;
		Connection connection = connectToDB.getDBConnection();
		Statement stmt = null;
		ResultSet rs = null;
		try 
		{
			stmt = connection.createStatement();
			rs = stmt.executeQuery("select ID from tripdetails td where td.TruckId = "+tankerId);
			if (rs == null)
			{
				throw new SQLException("Result set null");
			}
			while (rs.next())
			{
				tripID = rs.getInt("ID");
			}
		}
		catch (Exception e)
		{
			System.err.print("Exception while getting Trip Details "+e);
		}
		finally
		{
			try
			{
				if(null != stmt && null!= rs){
				stmt.close();
				rs.close();}
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return tripID;
	}
	
	public List<WaterDispenseDetails> getTripDetails()
	{
		List<WaterDispenseDetails> list = new ArrayList<WaterDispenseDetails>();
		Connection connection = connectToDB.getDBConnection();
		Statement stmt = null;
		ResultSet rs = null;
		try 
		{
			stmt = connection.createStatement();
			rs = stmt.executeQuery("SELECT td.NoOfPatches, td.ID, td.Status, td.TruckId, td.AreaId, tc.TruckNumber, td.WaterNeeded FROM tripdetails td,truck tc where td.TruckId=tc.ID and td.Status='PA'");
			if (rs == null)
			{
				throw new SQLException("Result set null");
			}
			while (rs.next())
			{
				WaterDispenseDetails details = new WaterDispenseDetails();
				details.setAreaName(String.valueOf(rs.getInt("AreaId")));
				details.setNoOfPatches(rs.getInt("NoOfPatches"));
				details.setTruckNumber(rs.getString("TruckNumber"));
				details.setWaterNeedDispense(rs.getInt("WaterNeeded"));
				details.setTruckId(rs.getInt("TruckId"));
				if(monitoringDetailsDao.setTripStatus(rs.getInt("ID"), "A") == 1)
					list.add(details);
				System.out.println(list);
			}
		}
		catch (Exception e)
		{
			System.err.print("Exception while getting Trip Details "+e);
		}
		finally
		{
			try
			{
				if(null != stmt && null!= rs){
				stmt.close();
				rs.close();
				}
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return list;
	}
	
	public String getTripStatus(int truckId)
	{
		List<WaterDispenseDetails> list = new ArrayList<WaterDispenseDetails>();
		Connection connection = connectToDB.getDBConnection();
		Statement stmt = null;
		ResultSet rs = null;
		try 
		{
			stmt = connection.createStatement();
			rs = stmt.executeQuery("SELECT * FROM tripdetails td,truck tc where td.TruckId=tc.ID and td.TruckId="+truckId);
			if (rs == null)
			{
				throw new SQLException("Result set null");
			}
			while (rs.next())
			{
				return rs.getString("Status");
			}
		}
		catch (Exception e)
		{
			System.err.print("Exception while getting Trip Details "+e);
		}
		finally
		{
			try
			{
				if(null != stmt && null!= rs){
				stmt.close();
				rs.close();
				}
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return "";
	}
	
	public int updateCoordinates(Coordinate coordinate, int tankerId) 
	{
		int success = 0;
		if(coordinate != null)
		{
		Connection con = connectToDB.getDBConnection();
		PreparedStatement preparedStatement = null;

		try {
			String query = "UPDATE truck td SET td.Coordinate = ? WHERE td.ID = ?";

			preparedStatement = con.prepareStatement(query);
			preparedStatement.setString(1, coordinate.getLat()+","+coordinate.getLng());
			preparedStatement.setInt(2, tankerId);
			
			success = preparedStatement.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if(null != preparedStatement)
					preparedStatement.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}}
		return success;
	}
	
	public int updateFlag(int tankerId) 
	{
		int success = 0;
		Connection con = connectToDB.getDBConnection();
		PreparedStatement preparedStatement = null;

		try {
			String query = "UPDATE truck td SET td.isMoving = ? WHERE td.ID = ?";

			preparedStatement = con.prepareStatement(query);
			preparedStatement.setString(1, "Y");
			preparedStatement.setInt(2, tankerId);
			
			success = preparedStatement.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if(null != preparedStatement)
					preparedStatement.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return success;
	}
	
	public String getIsMovingFlag(int tankerId)
	{
		String isMoving = null;
		Connection connection = connectToDB.getDBConnection();
		Statement stmt = null;
		ResultSet rs = null;
		try 
		{
			stmt = connection.createStatement();
			rs = stmt.executeQuery("select td.isMoving from truck td where td.ID = "+tankerId);
			if (rs == null)
			{
				throw new SQLException("Result set null");
			}
			while (rs.next())
			{
				isMoving = rs.getString("isMoving");
			}
		}
		catch (Exception e)
		{
			System.err.print("Exception while getting Trip Details "+e);
		}
		finally
		{
			try
			{
				if(null != stmt && null!= rs){
				stmt.close();
				rs.close();}
			}
			catch (SQLException e)
			{
				e.printStackTrace();
			}
		}
		return isMoving;
	}
	
}
