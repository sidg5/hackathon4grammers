package com.hackathon.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import com.hackathon.beans.Patch;
import com.hackathon.beans.Truck;

public class MonitoringDetailsDao {

	// List<Patch> patchList = new ArrayList<>();
	private ConnectToDB connectToDB = new ConnectToDB();

	/**
	 * to get patch coordinates
	 * 
	 * @param patchList
	 * @return
	 */
	public List<Patch> getCoordinates(List<Patch> patchList) {

		// List<in> areas = new ArrayList<>(2);
		Connection con = connectToDB.getDBConnection();
		Statement stmt = null;
		ResultSet rs = null;

		try {

			for (Patch patch : patchList) {

				stmt = con.createStatement();
				rs = stmt.executeQuery("select * from patch td where td.ID = "
						+ patch.getId());
				if (rs == null) {
					throw new SQLException("Result set null");
				}

				while (rs.next()) {
					patch.setCoordinates(rs.getString("Coordinates"));
					patch.setAreaId(rs.getInt("AreaId"));
					patch.setName(rs.getString("Name"));
					patch.setNoOfPlants(rs.getInt("NoOfPlants"));
					patch.setPlantCategory(rs.getString("PlantCategory"));
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if(stmt != null && rs!= null){
				stmt.close();
				rs.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return patchList;
	}

	public Truck getTruckDetails(int truckID) {
		Connection con = connectToDB.getDBConnection();
		Statement stmt = null;
		ResultSet rs = null;

		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery("select * from truck t where t.ID = "+truckID);
			if (rs == null) {
				throw new SQLException("Result set null");
			}

			while (rs.next()) {
				Truck truck = new Truck();

				truck.setId(truckID);
				truck.setAreaId(rs.getInt("AreaId"));
				truck.setCoordinate(rs.getString("Coordinate"));
				truck.setMaxCapacity(rs.getInt("MaxCapacity"));
				truck.setTruckNumber(rs.getString("TruckNumber"));
				truck.setUserId(rs.getInt("UserId"));
				truck.setWaterLeft(rs.getInt("WaterLeft"));

				return truck;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if(stmt != null && rs!= null){
				stmt.close();
				rs.close();}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return null;
	}

	public int setTripStatus(int tripID, String status) 
	{
		int success = 0;
		Connection con = connectToDB.getDBConnection();
		PreparedStatement preparedStatement = null;

		try {
			String query = "UPDATE tripdetails td SET td.Status = ? WHERE td.ID = ?";

			preparedStatement = con.prepareStatement(query);
			preparedStatement.setString(1, status);
			preparedStatement.setInt(2, tripID);
			
			success = preparedStatement.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if(preparedStatement != null){
				preparedStatement.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return success;
	}
	
	public String getRawTripCoordinates(int tripID) {

		Connection con = connectToDB.getDBConnection();
		PreparedStatement preparedStatement = null;
		Statement stmt = null;
		ResultSet rs = null;
		String coordinate = null;
		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery("select * from tripcoordinates tc where tc.tripId = "
					+ tripID);
			
			if (rs == null) {
				throw new SQLException("Result set null");
			}

			while (rs.next()) {
				coordinate = rs.getString("coordinates");
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if(stmt != null && rs!= null){
				stmt.close();
				rs.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return coordinate;
	}

	public void updatePatchStatus(int patchId, String status) {
		int success = 0;
		Connection con = connectToDB.getDBConnection();
		PreparedStatement preparedStatement = null;

		try {
			
			String query = "UPDATE waterneed td SET td.Status = ? WHERE td.PatchId = ?";
		//	String query = "UPDATE WaterNeeded td SET td.PatchId = ?";

			preparedStatement = con.prepareStatement(query);
			preparedStatement.setString(1, status);
			preparedStatement.setInt(2, patchId);
			
			success = preparedStatement.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if(preparedStatement != null){
				preparedStatement.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

	}

}
