package com.hackathon.dao;

import java.util.List;

import com.hackathon.beans.Coordinate;
import com.hackathon.beans.Patch;
import com.hackathon.beans.TankerTripDetails;
import com.hackathon.manager.MonitoringDetailManager;

public class TestApp {

	public static void main(String[] args) {
		MonitoringViewDao dao = new MonitoringViewDao();
		 List<TankerTripDetails> trips = dao.getAllTripsForArea(101);
		System.out.println("done");
		 
		MonitoringDetailManager detailManager = new MonitoringDetailManager();
		List<Patch> patchs = detailManager.getPatchesForTrip(1);
	
		
		List<Coordinate> coordinates = detailManager.getRouteCoodinates(1);
		System.out.println("done");
	}
}
