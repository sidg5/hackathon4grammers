package com.hackathon.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.hackathon.beans.Area;
import com.hackathon.beans.TankerTripDetails;

public class MonitoringViewDao {

	private ConnectToDB connectToDB = new ConnectToDB();

	public List<Area> getAllAreas() {

		List<Area> areas = new ArrayList<>(2);
		Connection con = connectToDB.getDBConnection();
		// here 132.186.96.129 : Roshni system ip
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = con.createStatement();
			rs = stmt.executeQuery("select * from area");
			if (rs == null) {
				throw new SQLException("Result set null");
			}

			while (rs.next()) {
				Area area = new Area();

				int id = rs.getInt("ID");
				area.setId(id);

				String areaName = rs.getString("AreaName");
				area.setAreaName(areaName);

				int noOfPatches = rs.getInt("NoOfPatches");
				area.setNoOfPatches(noOfPatches);

				int noOfTrucks = rs.getInt("NoOfTrucks");
				area.setNoOfTrucks(noOfTrucks);

				areas.add(area);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return areas;

	}

	public List<TankerTripDetails> getAllTripsForArea(int areaId) {

		List<TankerTripDetails> tripDetails = new ArrayList<>(2);

		Connection con = connectToDB.getDBConnection();

		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = con.createStatement();
			String query = "select t.ID, td.TruckId, td.NoOfPatches, td.PatchDetails, td.Status, t.TruckNumber, a.AreaName from tripdetails td, truck t, area a where td.AreaId = "+ areaId+" and td.TruckId = t.ID and td.AreaId = a.ID";
			rs = stmt.executeQuery(query);
			if (rs == null) {
				throw new SQLException("Result set null");
			}

			while (rs.next()) {
				TankerTripDetails tankerTripDetail = new TankerTripDetails();

				int id = rs.getInt("ID");
				tankerTripDetail.setId(id);

				int truckId = rs.getInt("TruckId");
				tankerTripDetail.setTruckid(truckId);
				tankerTripDetail.setTruckNumber(rs.getString("TruckNumber"));
				tankerTripDetail.setAreaName(rs.getString("AreaName"));
				int noOfPatches = rs.getInt("NoOfPatches");
				tankerTripDetail.setNoOfPatches(noOfPatches);

				String patchDetails = rs.getString("PatchDetails");
				tankerTripDetail.setPatchDetails(patchDetails);

				String status = rs.getString("Status");
				if(status.equals("PA"))
					status = "APPROVAL PENDING";
				else if(status.equals("A"))
					status = "APPROVED";
				else if(status.equals("C"))
					status = "COMPLETED";
				tankerTripDetail.setStatus(status);

				tankerTripDetail.setAreaID(areaId);

				tripDetails.add(tankerTripDetail);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return tripDetails;
	}

	public TankerTripDetails getTripDetailByTripID(int tripID) {

		// List<TankerTripDetails> tripDetails = new ArrayList<>(2);

		Connection con = connectToDB.getDBConnection();

		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = con.createStatement();
			String query = "select * from tripdetails  td where td.ID = "
					+ tripID;
			rs = stmt.executeQuery(query);
			if (rs == null) {
				throw new SQLException("Result set null");
			}

			while (rs.next()) {
				TankerTripDetails tankerTripDetail = new TankerTripDetails();

				int id = rs.getInt("ID");
				tankerTripDetail.setId(id);

				int truckId = rs.getInt("TruckId");
				tankerTripDetail.setTruckid(truckId);

				int noOfPatches = rs.getInt("NoOfPatches");
				tankerTripDetail.setNoOfPatches(noOfPatches);

				String patchDetails = rs.getString("PatchDetails");
				tankerTripDetail.setPatchDetails(patchDetails);

				String status = rs.getString("Status");
				tankerTripDetail.setStatus(status);

				tankerTripDetail.setAreaID(tripID);

				return tankerTripDetail;
				// tripDetails.add(tankerTripDetail);
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				stmt.close();
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return null;
	}

}
