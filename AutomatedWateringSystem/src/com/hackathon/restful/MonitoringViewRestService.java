package com.hackathon.restful;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.hackathon.beans.Area;
import com.hackathon.beans.TankerTripDetails;
import com.hackathon.dao.MonitoringViewDao;

@Path("/monitoringView") 
public class MonitoringViewRestService {

	@POST
	@Path("/fetchArea") 
	public Response fetchAreaDetail() {
		System.out.println("inside fetchRequestReplyTrips");
		MonitoringViewDao dao = new MonitoringViewDao();
		List<Area> allAreasList = dao.getAllAreas();
		
		ResponseBuilder builder = Response.status(200)
				.entity(allAreasList).type(MediaType.APPLICATION_JSON);

		return builder.build();
	}
	
	@POST
	@Path("/fetchTripDetails") 
	public Response fetchTripDetail( @FormParam("tripId") String tripId)
	{
		System.out.println("inside fetchTripDetail" +tripId);
		MonitoringViewDao dao = new MonitoringViewDao();
		List<TankerTripDetails> tankerTrips = new ArrayList<TankerTripDetails>();
		try
		{
			tankerTrips = dao.getAllTripsForArea(Integer.parseInt(tripId));
		}
		catch(NumberFormatException e)
		{
			System.out.println("Number format exception");
		}
		System.out.println(tankerTrips);
		ResponseBuilder builder = Response.status(200)
				.entity(tankerTrips).type(MediaType.APPLICATION_JSON);

		return builder.build();
	}
}
