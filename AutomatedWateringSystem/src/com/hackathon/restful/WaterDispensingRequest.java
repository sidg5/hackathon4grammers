/*
 * Copyright (c) Siemens AG 2017 ALL RIGHTS RESERVED.
 *
 * R8  
 * 
 */

package com.hackathon.restful;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.hackathon.beans.TankerTripDetails;
import com.hackathon.dao.ConnectToDB;

@Path("/waterdispensing")
public class WaterDispensingRequest {
	@POST
	@Path("/request")
	public Response addUser(@FormParam("truckid") String truckID) {

		System.out.println("truckID:" + truckID);
		ConnectToDB connectToDB = new ConnectToDB();
		Connection dbConnection = connectToDB.getDBConnection();

		TankerTripDetails tripDetail = new TankerTripDetails();
		tripDetail.setTruckid(Integer.parseInt(truckID));
		tripDetail.setStatus("PA");
		tripDetail.setWaterNeeded(getWaterNeeded());
		
		int i=0;
		getTripDetail(truckID, tripDetail, i);
		System.out.println("dbConnection:" + dbConnection);

		
		insertTripDetail(dbConnection, tripDetail);
		return Response.status(200)
				.entity(" Product added successfuly!<br> Id: " + truckID)
				.build();
	}

	private int insertTripDetail(Connection dbConnection,
			TankerTripDetails tripDetail) {
		
		int success = 0;
		//Connection con = dbConnection.getDBConnection();
		PreparedStatement preparedStatement = null;

		try {
			String query = "insert into tripdetails values (?,?,?,?,?,?,?)";

			preparedStatement = dbConnection.prepareStatement(query);
			int tripId = getTotalTripRows(dbConnection);
			
			preparedStatement.setInt(1, tripId);
			preparedStatement.setInt(2, tripDetail.getTruckid());	
			preparedStatement.setInt(3, tripDetail.getNoOfPatches());
			preparedStatement.setString(4, tripDetail.getPatchDetails());
			preparedStatement.setString(5, tripDetail.getStatus());
			preparedStatement.setInt(6, tripDetail.getAreaID());
			preparedStatement.setInt(7, tripDetail.getWaterNeeded());
			
			success = preparedStatement.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				preparedStatement.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return success;
	}

	private int getTotalTripRows(Connection dbConnection) {
		ResultSet rs = null;
		Statement stmt = null;
		int count  = 0;
		try {
			stmt = dbConnection.createStatement();
			rs = stmt.executeQuery("SELECT COUNT(*) FROM tripdetails");
			if (rs == null) {
				throw new SQLException("Result set null");
			}
			//tripDetail.setNoOfPatches(rs.get);
			while (rs.next()) {
				
				 count  = rs.getInt(1);
				// return truck;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			

		}
		return count+1;
		
	}

	private int getWaterNeeded() {
		// TODO Auto-generated method stub
		return 0;
	}

	public void getTripDetail(String truckID,
			TankerTripDetails tripDetail, int i) {

		Statement stmt = null;
		ResultSet rs = null;
		
		ConnectToDB connectToDB = new ConnectToDB();
		Connection dbConnection = connectToDB.getDBConnection();

		try {
			stmt = dbConnection.createStatement();
			rs = stmt.executeQuery("select * from truck td where td.ID = "+ truckID);
			if (rs == null)
			{
				throw new SQLException("Result set null");
			}
			//tripDetail.setNoOfPatches(rs.get);
			while (rs.next()) {
				++i;
				int areaId = rs.getInt("AreaId");
				tripDetail.setAreaID(areaId);
				tripDetail.setPatchDetails(getPatchDetails(areaId));
				
				// return truck;
			}
			tripDetail.setNoOfPatches(i);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			

		}
		// return null;
	}

	private String getPatchDetails(int areaId) {
		ResultSet rs = null;
		Statement stmt = null;
		String patchDetails = "";
		ConnectToDB connectToDB = new ConnectToDB();
		Connection dbConnection = connectToDB.getDBConnection();

		try {
			stmt = dbConnection.createStatement();
			rs = stmt.executeQuery("select * from patch td where td.AreaId = "+ areaId);
			if (rs == null) {
				throw new SQLException("Result set null");
			}

			while (rs.next()) {
				int patchID = rs.getInt("ID");
				patchDetails = patchID + ",";
				// return truck;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return patchDetails;
	}
}

/*
 * Copyright (c) Siemens AG 2017 ALL RIGHTS RESERVED
 * 
 * R8
 */
