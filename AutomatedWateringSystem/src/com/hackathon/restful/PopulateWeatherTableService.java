package com.hackathon.restful;

import java.sql.Connection;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.codehaus.jettison.json.JSONException;

import com.hackathon.Weather.WeatherDataParser;
import com.hackathon.beans.TankerTripDetails;
import com.hackathon.beans.WeatherBean;
import com.hackathon.dao.ConnectToDB;

@Path("/weatherService")
public class PopulateWeatherTableService {

	@POST
	@Path("/request")
	public Response addUser(@FormParam("weatherObject") String weatherObject) {

		System.out.println("weatherObject:" + weatherObject);
		ConnectToDB connectToDB = new ConnectToDB();
		Connection dbConnection = connectToDB.getDBConnection();

		WeatherDataParser weatherDataParser = new WeatherDataParser();
		try {
			
			WeatherBean weatherBean = new WeatherBean();
			double maxTemperatureForDay = weatherDataParser.getMaxTemperatureForDay(weatherObject, 1, weatherBean);
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Response.status(200)
				.entity(" Product added successfuly!<br> Id: " + weatherObject)
				.build();
	}

}
