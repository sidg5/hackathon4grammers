/*
 * Copyright (c) Siemens AG 2017 ALL RIGHTS RESERVED.
 *
 * R8  
 * 
 */

package com.hackathon.restful;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.hackathon.beans.Patch;
import com.hackathon.dao.ConnectToDB;

@Path("/userLogin")
public class TruckUserLogin {
	Connection dbConnection;

	public TruckUserLogin() {
		ConnectToDB connectToDB = new ConnectToDB();
		dbConnection = connectToDB.getDBConnection();
	}

	  @POST
	    @Path("/request")
	    public Response addUser(
	        @FormParam("userid") String userID,
	        @FormParam("password") String password,
	        @FormParam("userRole") String userRole)
	    {
	        
	        System.out.println("userID:" + userID +"password: "+password);
	        String truckID="";
	        Statement stmt;
	        try
	        {
	            stmt = dbConnection.createStatement();
	            ResultSet rs = stmt.executeQuery("SELECT * FROM userdetails WHERE  userid=\'"+userID+"\' AND password=\'"+password+"\' AND userRole=\'"+userRole+"\'");
	            System.out.println("rs:" + rs);
	            
	            if (rs != null)
	            {
	                while (rs.next())
	                {
	                    truckID = rs.getString("truckId");
	                    System.out.println(truckID + "\n");
	                }
	            }
	        }
	        catch (SQLException e)
	        {
	            // TODO Auto-generated catch block
	        }
	        String a = "";
	        if (userRole.equals("Driver"))
	        	a = "./pages/RemoteClientLogin.html?truckId="+truckID;
	        else if (userRole.equals("Dispenser"))
	        	a = "./pages/waterDispensingUI.html";
	        else if (userRole.equals("Monitor"))
	        	a = "./pages/monitoringView.html";
	        		
	        		
	        //window.location.href = "file2.html"
	        Response response = null;
	        if(!truckID.isEmpty())
	        {
	            response = Response.status(200)
	                .entity(a)
	                .build();
	        }
	        else
	        {
	            response = Response.status(400)
	            .entity("Truck ID not found for this user")
	            .build();
	        }
	        return response;
	    }
	    
	@POST
	@Path("/pollingPatchReach")
	public int pollingPatchReach(@FormParam("truckid") String truckid) {

		int patchID = 0;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Calendar cal = Calendar.getInstance();
		String format = dateFormat.format(cal.getTime());
		//Date date = format;
		System.out.println(format);

		// Connection con = dbConnection.getDBConnection();
		Statement stmt = null;
		ResultSet rs = null;
		try {
			stmt = dbConnection.createStatement();
			rs = stmt
					.executeQuery("select * from waterneed tc where tc.Status =\'R\' AND tc.Date= "+"\'"+format+"\'");
			//stmt.set

			if (rs != null) {

				while (rs.next()) {
					patchID = rs.getInt("PatchId");
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (stmt != null && rs != null) {
					stmt.close();
					rs.close();
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		System.out.println("patachID:"+patchID);
		return patchID;
	}
}

/*
 * Copyright (c) Siemens AG 2017 ALL RIGHTS RESERVED R8
 */
