package com.hackathon.restful;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.hackathon.beans.Coordinate;
import com.hackathon.beans.Patch;
import com.hackathon.manager.MonitoringDetailManager;

@Path("/MonitoringDetails")
public class MonitoringDetailsRestService {

	private MonitoringDetailManager monitoringDetailManager = new MonitoringDetailManager();

	@POST
	@Path("/getPatchCoordinates")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Coordinate> getPatchCoordinates(@FormParam("id") int tripID) {

		List<Coordinate> coordinates = new ArrayList<>();

		List<Patch> patches = monitoringDetailManager
				.getPatchesForTrip(tripID);
		for (Patch patch : patches) {

			Coordinate coordinate = new Coordinate();
			String latLng = patch.getCoordinates();
			String[] latAndLng = latLng.split(",");
			coordinate.setLat(Double.parseDouble(latAndLng[0]));
			coordinate.setLng(Double.parseDouble(latAndLng[1]));
			coordinates.add(coordinate);
		}
		return coordinates;
	}
	
	@POST
	@Path("/getPatches")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Patch> getPatches(@FormParam("id") int tripID) {

		List<Coordinate> coordinates = new ArrayList<>();

		List<Patch> patches = monitoringDetailManager
				.getPatchesForTrip(tripID);
		
		return patches;
	}


	@POST
	@Path("/getTankerLocation")
	@Produces({ MediaType.APPLICATION_JSON })
	public Coordinate getTankerLocation(@FormParam("id") int tankerId) {
		if(tankerId== 0){return null;}
		Coordinate coordinate = monitoringDetailManager
				.getTruckCurrentPosition(tankerId);
		
		return coordinate;
	}
	
	@POST
	@Path("/updatePatchStatus")
	@Produces({ MediaType.APPLICATION_JSON })
	public Patch updatePatchStatus(@FormParam("id") int patchId) {

		Patch patch = monitoringDetailManager
				.updatePatchStatus(patchId,"R");
		
		return patch;
	}
	
}
