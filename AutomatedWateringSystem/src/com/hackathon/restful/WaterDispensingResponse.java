package com.hackathon.restful;


import java.util.List;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackathon.beans.Coordinate;
import com.hackathon.beans.WaterDispenseDetails;
import com.hackathon.dao.FetchTripDetailsDao;
import com.hackathon.manager.MonitoringDetailManager;

@Path("/handleDispenseRequest")
public class WaterDispensingResponse 
{
	private static final transient ObjectMapper MAPPER = new ObjectMapper();

	@POST
	@Path("/fetchTrips")
	public Response fetchRequestReplyTrips()
	{
		ResponseBuilder builder = Response.status(200).entity(fetchTripsFromDB()).type(MediaType.APPLICATION_JSON);
		return builder.build();
	}
	
	@POST
	@Path("/fetchStatus")
	public Response fetchRequestReplyTrips( @FormParam("tankerId") String tankerId)
	{
		FetchTripDetailsDao dao = new FetchTripDetailsDao();
		String status = dao.getTripStatus(Integer.parseInt(tankerId));
		if(status.equalsIgnoreCase("A"))
			return Response.status(200).entity("A").type(MediaType.APPLICATION_JSON).build();
		else
			return Response.status(200).entity("NA").type(MediaType.APPLICATION_JSON).build();
	}

	@POST
	@Path("/initiateTrip")
	public Response initiateTrip( @FormParam("tankerId") String tankerId)
	{
		FetchTripDetailsDao dao = new FetchTripDetailsDao();
		dao.updateFlag(Integer.parseInt(tankerId));
		
		updateCoordinates(tankerId);
		
		ResponseBuilder builder = Response.status(200).entity(fetchTripsFromDB()).type(MediaType.APPLICATION_JSON);
		return builder.build();
	}
	
	private synchronized String fetchTripsFromDB()
	{
		FetchTripDetailsDao fetchTripDao = new FetchTripDetailsDao();
		List<WaterDispenseDetails> details = fetchTripDao.getTripDetails();
		System.out.println("Details "+details);
		try
		{
			return MAPPER.writeValueAsString(details);
		}
		catch (JsonProcessingException e)
		{
			e.printStackTrace();
		}
		return "dummy";
	}
	
	@POST
	@Path("/updateCoordinates")
	public void updateCoordinates( @FormParam("tankerId") String tankerId)
	{
		System.out.println("UpdateCoordinates"+tankerId);
		FetchTripDetailsDao dao = new FetchTripDetailsDao();
		int tripId = dao.getTripId(Integer.parseInt(tankerId));
		MonitoringDetailManager manager = new MonitoringDetailManager();
		List<Coordinate> coordinates = manager.getRouteCoodinates(Integer.parseInt(tankerId));
		
		for (Coordinate coordinate : coordinates)
		{
			if (dao.getIsMovingFlag(Integer.parseInt(tankerId)).equals("Y"))
				dao.updateCoordinates(coordinate, Integer.parseInt(tankerId));
			try
			{
				Thread.sleep(1000);
			}
			catch (InterruptedException e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}