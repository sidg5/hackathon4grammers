package com.hackathon.Weather;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import com.hackathon.beans.WeatherBean;

public class WeatherDataParser {

	/**
	 * Given a string of the form returned by the api call:
	 * http://api.openweathermap.org/data/2.5/forecast/daily?q=94043&mode=json&units=metric&cnt=7 
	 * retrieve the maximum temperature for the day indicated by dayIndex
	 * (Note: 0-indexed, so 0 would refer to the first day).
	 * @param weatherBean 
	 */
	public static double getMaxTemperatureForDay(String weatherJsonStr, int dayIndex, WeatherBean weatherBean)
		throws JSONException {
		// TODO: add parsing code here
        //getting the whole JSON Object
        JSONObject jsonObj = new JSONObject(weatherJsonStr);
        //getting the list node
        JSONArray list=jsonObj.getJSONArray("list");
        //getting the required element from list by dayIndex
        JSONObject item=list.getJSONObject(dayIndex);
        int humidity = item.getInt("humidity");
        
        //getting the temp node in that element
        JSONObject temp=item.getJSONObject("temp");
        //getting the max atribute from temp object
        int max= temp.getInt("max");
        //int rain = item.
        
        weatherBean.setTemperature(max);
        weatherBean.setHumidity(humidity);
        
        
		return  max;
	}
	}
