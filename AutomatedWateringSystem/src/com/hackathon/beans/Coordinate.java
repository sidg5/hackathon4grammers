package com.hackathon.beans;

public class Coordinate {

	private double lat;
	private double lng;
	private boolean isTankerAtPatch;
	
	public double getLat() {
		return lat;
	}
	public void setLat(double lat) {
		this.lat = lat;
	}
	public double getLng() {
		return lng;
	}
	public void setLng(double lng) {
		this.lng = lng;
	}
	public boolean isTankerAtPatch() {
		return isTankerAtPatch;
	}
	public void setTankerAtPatch(boolean isTankerAtPatch) {
		this.isTankerAtPatch = isTankerAtPatch;
	}
	
	
	
}
