package com.hackathon.beans;

public class Truck {
	private int id;
	private int userId;
	private int maxCapacity;
	private String coordinate;
	private int areaId;
	private int waterLeft;
	private String truckNumber;
	private String isMoving;

	public String getIsMoving() {
		return isMoving;
	}

	public void setIsMoving(String isMoving) {
		this.isMoving = isMoving;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getMaxCapacity() {
		return maxCapacity;
	}

	public void setMaxCapacity(int maxCapacity) {
		this.maxCapacity = maxCapacity;
	}

	public String getCoordinate() {
		return coordinate;
	}

	public void setCoordinate(String coordinate) {
		this.coordinate = coordinate;
	}

	public int getAreaId() {
		return areaId;
	}

	public void setAreaId(int areaId) {
		this.areaId = areaId;
	}

	public int getWaterLeft() {
		return waterLeft;
	}

	public void setWaterLeft(int waterLeft) {
		this.waterLeft = waterLeft;
	}

	public String getTruckNumber() {
		return truckNumber;
	}

	public void setTruckNumber(String truckNumber) {
		this.truckNumber = truckNumber;
	}

}
