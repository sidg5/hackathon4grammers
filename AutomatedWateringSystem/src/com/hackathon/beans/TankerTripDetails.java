package com.hackathon.beans; 

public class TankerTripDetails
{
	private int id;
	private int truckid;
	private int noOfPatches;
	private String patchDetails;
	private String status;
	private int areaID;
	private int waterNeeded;
	private String truckNumber;
	public String getTruckNumber() {
		return truckNumber;
	}
	public void setTruckNumber(String truckNumber) {
		this.truckNumber = truckNumber;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	private String areaName;
	
	
	public int getWaterNeeded() {
		return waterNeeded;
	}
	public void setWaterNeeded(int waterNeeded) {
		this.waterNeeded = waterNeeded;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getTruckid() {
		return truckid;
	}
	public void setTruckid(int truckid) {
		this.truckid = truckid;
	}
	public int getNoOfPatches() {
		return noOfPatches;
	}
	public void setNoOfPatches(int noOfPatches) {
		this.noOfPatches = noOfPatches;
	}
	public String getPatchDetails() {
		return patchDetails;
	}
	public void setPatchDetails(String patchDetails) {
		this.patchDetails = patchDetails;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public int getAreaID() {
		return areaID;
	}
	public void setAreaID(int areaID) {
		this.areaID = areaID;
	}
	
	
}