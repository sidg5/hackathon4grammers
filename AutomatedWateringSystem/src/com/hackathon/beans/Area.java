package com.hackathon.beans;

public class Area {

	private int id;
	private String areaName;
	private int noOfPatches;
	private int noOfTrucks;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public int getNoOfPatches() {
		return noOfPatches;
	}
	public void setNoOfPatches(int noOfPatches) {
		this.noOfPatches = noOfPatches;
	}
	public int getNoOfTrucks() {
		return noOfTrucks;
	}
	public void setNoOfTrucks(int noOfTrucks) {
		this.noOfTrucks = noOfTrucks;
	}
	
	
	
}
