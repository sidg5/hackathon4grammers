/**
 * 
 * 
 */
 var truckId = null;
$(document).ready(function () {
	 var rootURL = "http://localhost:8080/hackathon4grammers/rest/wines/post";
	
	 $('#sampleButton').click(function (event) {
	 
		 $.ajax({
				type: 'POST',
				contentType: 'application/json',
				url: rootURL,
				dataType: "json",
				data: formToJSON(),
				success: function(data, textStatus, jqXHR){
					alert(data.id);
				},
				error: function(jqXHR, textStatus, errorThrown){
					alert('addWine error: ' + textStatus);
				}
			});
		
	
	 });
	 
	 $('#tankFill').click(function (event) {
		 truckId = findGetParameter('truckId');
		 $.ajax({
			   url: 'http://localhost:8080/AutomatedWateringSystem/rest/waterdispensing/request',
			   data: {
			      format: 'json',
			      'truckid': truckId
			   },
			   success: function(data) {
				   //alert("success");
				 setInterval(startPollingForReachingPatch,2000);
			   },
			   type: 'POST'
			});
	
	 });
	 
	 function findGetParameter(parameterName) {
		    var result = null,
		        tmp = [];
		    var items = location.search.substr(1).split("&");
		    for (var index = 0; index < items.length; index++) {
		        tmp = items[index].split("=");
		        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
		    }
		    return result;
		}
	
	 $('#loginReq').click(function (event) {
		
		 var userid =$('#userid').val();
		 var password =$('#password').val();
		 var userRole =$('#userRole').val();
		 
		 $.ajax({
			   url: 'http://localhost:8080/AutomatedWateringSystem/rest/userLogin/request',
			   data: {
			      format: 'json',
			      'userid': userid,
			      'password': password,
			      'userRole': userRole
			   },
			   success: function(data)
			   {
				   window.location.href = data;
			   },
			   error: function(data)
			   {
				   handleError();
			   },
			   type: 'POST'
			});
	
	 });
	 $('#goToPatch').click(function (event) {

		 var tankerId = findGetParameter('truckId');

		 $.ajax({

		 url: 'http://localhost:8080/AutomatedWateringSystem/rest/handleDispenseRequest/initiateTrip',

		 data:

		 {

		 format: 'json',

		 'tankerId': tankerId

		 },

		 success: function(data)

		 {

		 alert(data);

		 },

		 error: function(data)

		 {

		 },

		 type: 'POST'

		 });


		 });

	 function startPollingForReachingPatch(){
		 var truckId = findGetParameter('truckId');
		 $.ajax({
			   url: 'http://localhost:8080/AutomatedWateringSystem/rest/pollingPatchReach',
			   data: {
			      format: 'json',
			      'truckid': truckId
			   },
			   success: function(patch)
			   {
				   if(patch.status == 'R'){
					   alert('Tanker has reached the patch location ' + patch.name +'<br> Please initiate Patch Filling Request');
				   }
			   },
			   error: function(data)
			   {
				   handleError();
			   },
			   type: 'POST'
			});
	 }
});