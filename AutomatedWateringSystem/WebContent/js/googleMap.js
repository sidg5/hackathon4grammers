/**
 * 
 */


 var locations = null;
    
    var truckCoordinate = 0;
    var map = null;
    var tankerMarker = null;
    var directionsService = null;
    var directionsDisplay = null;
    
	var cityCircles = null;
	
	$(document).ready(function () {
	  	//variable to be replaced by URl param todo
    	var tripId = 1;
		locations = getPatchLocations(tripId);
		
	});
	
	//Automatic called from Google API
    function initMap() {
    	
        directionsService = new google.maps.DirectionsService;
        directionsDisplay = new google.maps.DirectionsRenderer(
        		  {
        		      suppressMarkers: true
         });
       	 
    	var style = getStylers();

	var indiaGate = {
		lat : 28.612828,
		lng : 77.231144
	};
	map = new google.maps.Map(document.getElementById('map'), {
		zoom : 17,
		center : indiaGate,
		mapTypeId : google.maps.MapTypeId.MAP,
		styles : style
	});

	// Create an array of alphabetical characters used to label the markers.
	var labels = [ "Patch 1", "Patch 2", "Patch 3", "Patch 4", "Patch 5",
			"Patch 6", "Patch 7", "Patch 8", "Patch 9", "Patch 10" ];

	var image = {
		url : 'sign.jpg',
		// This marker is 20 pixels wide by 32 pixels high.
		size : new google.maps.Size(60, 60),
		// The origin for this image is (0, 0).
		origin : new google.maps.Point(0, 0),
		// The anchor for this image is the base of the flagpole at (0, 32).
		anchor : new google.maps.Point(30, 60)
	};

	// Add some markers to the map.
	// Note: The code uses the JavaScript Array.prototype.map() method to
	// create an array of markers based on a given "locations" array.
	// The map() method here has nothing to do with the Google Maps API.
	/*
	 * var markers = locations.map(function(location, i) { return new
	 * google.maps.Marker({ position: location, label: labels[i %
	 * labels.length], }); });
	 */

	var infowindow = new google.maps.InfoWindow({
		content : "Dummy content string specific to marker",
		maxWidth : 200
	});

	var markers = new Array();  

	 $.each(locations, function(location, i) {

			var marker = new google.maps.Marker({
				position : location,
				label : labels[i % labels.length],
			});

			marker.addListener('click', function() {
				infowindow.open(map, marker);
			});

			markers.push(marker);
		});
	
	// Add a marker clusterer to manage the markers.
	var markerCluster = new MarkerClusterer(
			map,
			markers,
			{
				imagePath : 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'
			});

	/* Circle portion starts from here */
	var cityCircles = new Array();
	
	 $.each(locations, function(location, i) {
		 var circle  = new google.maps.Circle({
				strokeColor : '#0000FF',
				strokeOpacity : 0.5,
				strokeWeight : 1,
				fillColor : '#0000FF',
				fillOpacity : 0.01,
				map : map,
				center : location,
				radius : 25
			});
		 cityCircles.push(circle);		
		});

	// Set Tanker
	setTankerMarkers(map);
	
	//Create Route
	createRoute(map);
   }
	
    
    function getStylers(){
    	
    	var style =   [
    	               /*
						 * {elementType: 'geometry', stylers: [{color:
						 * '#242f3e'}]}, {elementType: 'labels.text.stroke',
						 * stylers: [{color: '#242f3e'}]}, {elementType:
						 * 'labels.text.fill', stylers: [{color: '#746855'}]},
						 */ 
    	                        {
    	                          featureType: 'administrative',
    	                          elementType: 'all',
    	                          stylers: [{"visibility": "off"}]
    	                        },
    	                        {
    	                          featureType: 'poi',
    	                          elementType: 'all',
    	                          stylers: [{"visibility": "off"}]
    	                        },
    	                        /* {
    	                          featureType: 'road',
    	                          elementType: 'geometry',
    	                          stylers: [{color: '#38414e'}]
    	                        },
    	                        {
    	                          featureType: 'road',
    	                          elementType: 'geometry.stroke',
    	                          stylers: [{color: '#212a37'}]
    	                        },
    	                        {
    	                          featureType: 'road',
    	                          elementType: 'labels.text.fill',
    	                          stylers: [{color: '#9ca5b3'}]
    	                        },
    	                        {
    	                          featureType: 'road.highway',
    	                          elementType: 'geometry',
    	                          stylers: [{color: '#746855'}]
    	                        },
    	                        {
    	                          featureType: 'road.highway',
    	                          elementType: 'geometry.stroke',
    	                          stylers: [{color: '#1f2835'}]
    	                        },
    	                        {
    	                          featureType: 'road.highway',
    	                          elementType: 'labels.text.fill',
    	                          stylers: [{color: '#f3d19c'}]
    	                        },
    	                        {
    	                          featureType: 'transit',
    	                          elementType: 'geometry',
    	                          stylers: [{color: '#2f3948'}]
    	                        },
    	                        {
    	                          featureType: 'transit.station',
    	                          elementType: 'labels.text.fill',
    	                          stylers: [{color: '#d59563'}]
    	                        } ,*/
    	                        {
    	                          featureType: 'water',
    	                          elementType: 'all',
    	                          stylers: [{"visibility": "off"}]
    	                        }
    	                      ];
    	
    	return style;
    }
    
    function setTankerMarkers(map) {
        // Adds markers to the map.

        // Marker sizes are expressed as a Size of X,Y where the origin of the image
        // (0,0) is located in the top left of the image.

        // Origins, anchor positions and coordinates of the marker increase in the X
        // direction to the right and in the Y direction down.
        var image = {
          url: '../images/truck_gmap.png',
          // This marker is 20 pixels wide by 32 pixels high.
          size: new google.maps.Size(50, 50),
          // The origin for this image is (0, 0).
          origin: new google.maps.Point(0, 0),
          // The anchor for this image is the base of the flagpole at (0, 32).
          anchor: new google.maps.Point(0, 50)
        };
        // Shapes define the clickable region of the icon. The type defines an HTML
        // <area> element 'poly' which traces out a polygon as a series of X,Y points.
        // The final coordinate closes the poly by connecting to the first coordinate.
        var shape = {
          coords: [1, 1, 1, 20, 18, 20, 18, 1],
          type: 'poly'
        };

        var tanker  = {lat: 28.615161, lng: 77.228764};
        tankerMarker = new google.maps.Marker({
            position: tanker,
            map: map,
            icon: image,
            shape: shape,
            title: "Tanker"
          });
        
       setInterval(updateTankerLocation, 500);
    			
    }
    
    function updateTankerLocation(){
    	var curPos = getTruckCurrentPosition();
    	tankerMarker.setPosition(curPos);
    	
    	//Check for Tanker pos
    	var curLatLng = new google.maps.LatLng(curPos.lat, curPos.lng);   
    	
    	$(cityCircles).each(function( index, circle ) {
      		 if((google.maps.geometry.spherical.computeDistanceBetween(curLatLng, circle.center) <= circle.radius)){
      			 alert('got it');
      		 }
      		 
      		 //By Pass further alerts once reached to a destination
      		 while((google.maps.geometry.spherical.computeDistanceBetween(curLatLng, circle.center) <= circle.radius)){
      			curPos = getTruckCurrentPosition();
      			curLatLng = new google.maps.LatLng(curPos.lat, curPos.lng);  
      		 }
      		 
    	  });    
    }
    
    function getTruckCurrentPosition(){
    	
    	
    	var coordinates = [{ lat: 28.61306, lng: 77.2277}
		,{ lat: 28.61426, lng: 77.22775}
		,{ lat: 28.61426, lng: 77.22775}
		,{ lat: 28.61435, lng: 77.2278}
		,{ lat: 28.61444, lng: 77.22784}
		,{ lat: 28.61456, lng: 77.22791}
		,{ lat: 28.61465, lng: 77.22798}
		,{ lat: 28.61474, lng: 77.22804}
		,{ lat: 28.61482, lng: 77.22814}
		,{ lat: 28.61489, lng: 77.22824}
		,{ lat: 28.61499, lng: 77.22839}
		,{ lat: 28.61505, lng: 77.22849}
		,{ lat: 28.61529, lng: 77.22904}
		,{ lat: 28.61538, lng: 77.22922}
		,{ lat: 28.61539, lng: 77.22924}
		,{ lat: 28.61542, lng: 77.2293}
		,{ lat: 28.61551, lng: 77.22952}
		,{ lat: 28.61557, lng: 77.22966}
		,{ lat: 28.61568, lng: 77.22989}
		,{ lat: 28.61569, lng: 77.22991}
		,{ lat: 28.6161, lng: 77.2308}
		,{ lat: 28.6161, lng: 77.2308}
		,{ lat: 28.61615, lng: 77.23092}
		,{ lat: 28.61621, lng: 77.23109}
		,{ lat: 28.61623, lng: 77.23121}
		,{ lat: 28.61623, lng: 77.2313}
		,{ lat: 28.61622, lng: 77.23138}
		,{ lat: 28.6162, lng: 77.23147}
		,{ lat: 28.61618, lng: 77.23156}
		,{ lat: 28.61611, lng: 77.23172}
		,{ lat: 28.61595, lng: 77.23203}
		,{ lat: 28.61594, lng: 77.23203}
		,{ lat: 28.61569, lng: 77.23249}
		,{ lat: 28.61563, lng: 77.23259}
		,{ lat: 28.61563, lng: 77.23259}
		,{ lat: 28.61554, lng: 77.23272}
		,{ lat: 28.61538, lng: 77.23299}
		,{ lat: 28.61518, lng: 77.23333}
		,{ lat: 28.61512, lng: 77.23344}
		,{ lat: 28.61468, lng: 77.2342}
		,{ lat: 28.61468, lng: 77.2342}
		,{ lat: 28.61464, lng: 77.23424}
		,{ lat: 28.61454, lng: 77.23434}
		,{ lat: 28.61447, lng: 77.23443}
		,{ lat: 28.6144, lng: 77.23449}
		,{ lat: 28.61435, lng: 77.23452}
		,{ lat: 28.61433, lng: 77.23454}
		,{ lat: 28.61423, lng: 77.2346}
		,{ lat: 28.61414, lng: 77.23462}
		,{ lat: 28.6139, lng: 77.23463}
		,{ lat: 28.61278, lng: 77.23457}
		,{ lat: 28.61252, lng: 77.23456}
		,{ lat: 28.61134, lng: 77.23447}
		,{ lat: 28.61134, lng: 77.23447}
		,{ lat: 28.61124, lng: 77.23444}
		,{ lat: 28.61117, lng: 77.23441}
		,{ lat: 28.61108, lng: 77.23437}
		,{ lat: 28.611, lng: 77.23432}
		,{ lat: 28.61094, lng: 77.23427}
		,{ lat: 28.61086, lng: 77.2342}
		,{ lat: 28.61079, lng: 77.23411}
		,{ lat: 28.61071, lng: 77.234}
		,{ lat: 28.61065, lng: 77.23385}
		,{ lat: 28.61048, lng: 77.23345}
		,{ lat: 28.61023, lng: 77.23287}
		,{ lat: 28.61023, lng: 77.23287}
		,{ lat: 28.61014, lng: 77.23268}
		,{ lat: 28.61004, lng: 77.23246}
		,{ lat: 28.61001, lng: 77.2324}
		,{ lat: 28.60996, lng: 77.23229}
		,{ lat: 28.60984, lng: 77.23201}
		,{ lat: 28.60962, lng: 77.23154}
		,{ lat: 28.60956, lng: 77.23138}
		,{ lat: 28.60955, lng: 77.23135}
		,{ lat: 28.60955, lng: 77.23135}
		,{ lat: 28.60952, lng: 77.23118}
		,{ lat: 28.6095, lng: 77.23103}
		,{ lat: 28.6095, lng: 77.23086}
		,{ lat: 28.60951, lng: 77.23076}
		,{ lat: 28.60953, lng: 77.23063}
		,{ lat: 28.60958, lng: 77.23048}
		,{ lat: 28.60964, lng: 77.2303}
		,{ lat: 28.60965, lng: 77.23028}
		,{ lat: 28.61002, lng: 77.22969}
		,{ lat: 28.61004, lng: 77.22966}
		,{ lat: 28.61005, lng: 77.22963}
		,{ lat: 28.6102, lng: 77.22938}];
    	
    	if(truckCoordinate == coordinates.length){
    		truckCoordinate = 0;
    	}
    return coordinates[truckCoordinate++];
    }
    
    function createRoute(){
    
    	directionsDisplay.setMap(map);
    	
    	var patch1 = new google.maps.LatLng(28.614765, 77.227806);
       	var patch6 = new google.maps.LatLng(28.611216, 77.227632);
       	
    	var patch2 = new google.maps.LatLng(28.616381, 77.231332);
       	var patch3 = new google.maps.LatLng(28.614395, 77.234638);
       	
    	var wayPoints = [{
            location: patch2,
            stopover: false
          },{
              location: patch3,
              stopover: true
            }];   	
    	
    	
    	var request = {
    		      origin: patch1,
    		      destination: patch6,
    		      // Note that Javascript allows us to access the constant
    		      // using square brackets and a string value as its
    		      // "property."
    		      travelMode: 'DRIVING',
    		      waypoints: wayPoints
    		  };
    		  directionsService.route(request, function(response, status) {
    		    if (status == 'OK') {
    		      directionsDisplay.setDirections(response);
    		    } else {
    	            alert('Directions request failed due to ' + status);
    	        }
    		  });
    		
    }
    
    
    //Changes done permanently
    function getPatchLocations(tripId){
    	 var tripId = 1;
		 var mourl = "http://localhost:8080/AutomatedWateringSystem/rest/MonitoringDetails/getPatches";
    	 $.ajax({
    		 	url: mourl,
				dataType: "json",
				data: {
					 format: 'json',
				      'id': tripId
				},
				 type: 'POST',
				success: function(data, textStatus, jqXHR){
					locations = data;
				},
				error: function(jqXHR, textStatus, errorThrown){
					alert('addWine error: ' + textStatus);
				},
			   
			});
    }
    
    function formToJSON(tripId) {
		return JSON.stringify({
				"id":tripId
				});
		}
    
    