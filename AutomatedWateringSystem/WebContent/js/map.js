/**
 * 
 */
    
 	var markers  = null;
    var truckCoordinate = 0;
    var map = null;
    var tankerMarker = null;
    var directionsService = null;
    var directionsDisplay = null;
    var tripId = 1;
    var tankerId = 1001;
	var cityCircles = null;
	var patches = [];
	var indiaGate = {
			lat : 28.612828,
			lng : 77.231144
		};
	var infowindow = null;
    var isTankerReachedDestination = false;
	
    function initMap() {   	
    	
        directionsService = new google.maps.DirectionsService;
        directionsDisplay = new google.maps.DirectionsRenderer(
        		  {
        		      suppressMarkers: true
         });
       	 
    	var style = getStylers();

    	  tankerId = findGetParameter('tankerId');
    	  tripId = findGetParameter('tripId');
    	 
	map = new google.maps.Map(document.getElementById('map'), {
		zoom : 17,
		center : indiaGate,
		mapTypeId : google.maps.MapTypeId.MAP,
		styles : style
	});

	
	

	// Add some markers to the map.
	// Note: The code uses the JavaScript Array.prototype.map() method to
	// create an array of markers based on a given "locations" array.
	// The map() method here has nothing to do with the Google Maps API.
	/*
	 * var markers = locations.map(function(location, i) { return new
	 * google.maps.Marker({ position: location, label: labels[i %
	 * labels.length], }); });
	 */
	
	tankerMarker = new google.maps.Marker({
		position : indiaGate,
		map: map,
	});
	
	
	//Call patch location
	getPatchLocations(tripId);

	setInitialTruckPoistion();
	
	//Update Truck Location from DB
	setInterval(updateTankerPosition,10000);
   }
	
    
    function getStylers(){
    	
    	var style =   [
    	               /*
						 * {elementType: 'geometry', stylers: [{color:
						 * '#242f3e'}]}, {elementType: 'labels.text.stroke',
						 * stylers: [{color: '#242f3e'}]}, {elementType:
						 * 'labels.text.fill', stylers: [{color: '#746855'}]},
						 */ 
    	                        {
    	                          featureType: 'administrative',
    	                          elementType: 'all',
    	                          stylers: [{"visibility": "off"}]
    	                        },
    	                        {
    	                          featureType: 'poi',
    	                          elementType: 'all',
    	                          stylers: [{"visibility": "off"}]
    	                        },
    	                        /* {
    	                          featureType: 'road',
    	                          elementType: 'geometry',
    	                          stylers: [{color: '#38414e'}]
    	                        },
    	                        {
    	                          featureType: 'road',
    	                          elementType: 'geometry.stroke',
    	                          stylers: [{color: '#212a37'}]
    	                        },
    	                        {
    	                          featureType: 'road',
    	                          elementType: 'labels.text.fill',
    	                          stylers: [{color: '#9ca5b3'}]
    	                        },
    	                        {
    	                          featureType: 'road.highway',
    	                          elementType: 'geometry',
    	                          stylers: [{color: '#746855'}]
    	                        },
    	                        {
    	                          featureType: 'road.highway',
    	                          elementType: 'geometry.stroke',
    	                          stylers: [{color: '#1f2835'}]
    	                        },
    	                        {
    	                          featureType: 'road.highway',
    	                          elementType: 'labels.text.fill',
    	                          stylers: [{color: '#f3d19c'}]
    	                        },
    	                        {
    	                          featureType: 'transit',
    	                          elementType: 'geometry',
    	                          stylers: [{color: '#2f3948'}]
    	                        },
    	                        {
    	                          featureType: 'transit.station',
    	                          elementType: 'labels.text.fill',
    	                          stylers: [{color: '#d59563'}]
    	                        } ,*/
    	                        {
    	                          featureType: 'water',
    	                          elementType: 'all',
    	                          stylers: [{"visibility": "off"}]
    	                        }
    	                      ];
    	
    	return style;
    }
    
  //Changes done permanently
    function getPatchLocations(tripId){
    	 var tripId = 1;
		 var mourl = "http://localhost:8080/AutomatedWateringSystem/rest/MonitoringDetails/getPatches";
    	 $.ajax({
    		 	url: mourl,
				dataType: "json",
				data: {
					 format: 'json',
				      'id': tripId
				},
				type: 'POST',
				success: function(data, textStatus, jqXHR){

					var startPatch = null;
					var endPatch = null;
				
					$.each( data,function(i, patch) {
						// Create an array of alphabetical characters used to label the markers.
						var myLatLng = {lat: patch.coordinate.lat, lng: patch.coordinate.lng};
						var marker = new google.maps.Marker({
							position : myLatLng,
							map: map,
							label : patch.name,
						});

						infowindow = new google.maps.InfoWindow({
							content : "<b>Patch Name: </b>"+patch.name+" <br> <b>Total No of Plants :: </b>"+patch.noOfPlants+" <br> <b>Category Of Plants :: </b>"+patch.plantCategory,
							maxWidth : 200
						});
						 
						marker.addListener('click', function() {
							infowindow.open(map, marker);
						});


						var circle  = new google.maps.Circle({
								strokeColor : '#0000FF',
								strokeOpacity : 0.5,
								strokeWeight : 1,
								fillColor : '#0000FF',
								fillOpacity : 0.01,
								map : map,
								center : myLatLng,
								radius : 25
						});
						
						if(i == 0){
							startPatch = patch;
						}
						
						if(i == data.length-1){
							endPatch = patch;
						}
						patches.push(patch);
					});
					
					createRoute(startPatch,endPatch);
					
				},
				error: function(jqXHR, textStatus, errorThrown){
					//alert('addWine error: ' + textStatus);
				}
			   
			});
    }
    
    function setInitialTruckPoistion(){
    	 var image = {
    	          url: '../images/truck_gmap.png',
    	          // This marker is 20 pixels wide by 32 pixels high.
    	          size: new google.maps.Size(50, 50),
    	          // The origin for this image is (0, 0).
    	          origin: new google.maps.Point(0, 0),
    	          // The anchor for this image is the base of the flagpole at (0, 32).
    	          anchor: new google.maps.Point(0, 50)
    	        };
    	        // Shapes define the clickable region of the icon. The type defines an HTML
    	        // <area> element 'poly' which traces out a polygon as a series of X,Y points.
    	        // The final coordinate closes the poly by connecting to the first coordinate.
    	        var shape = {
    	          coords: [1, 1, 1, 20, 18, 20, 18, 1],
    	          type: 'poly'
    	        };

    	       tankerMarker = new google.maps.Marker({
    	            position: indiaGate,
    	            map: map,
    	            icon: image,
    	            shape: shape,
    	            title: "Tanker"
    	          });
    	        
    }
    
    function updateTankerPosition(){
    	
    	 var mourl = "http://localhost:8080/AutomatedWateringSystem/rest/MonitoringDetails/getTankerLocation";
    	var tankerId = findGetParameter('tankerId');
    	 $.ajax({
    		 	url: mourl,
				dataType: "json",
				data: {
					 format: 'json',
				      'id': tankerId
				},
				type: 'POST',
				success: function(data, textStatus, jqXHR){
					var myLatLng = {lat: data.lat, lng: data.lng};
					tankerMarker.setPosition(myLatLng);
					checkIfTankerReachedPatch(myLatLng);
					if(!data.isTankerAtPatch){
						
						
					}
				
				},
				error: function(jqXHR, textStatus, errorThrown){
				//	alert('addWine error: ' + textStatus);
				}
			   
			});
    }
    

	 function findGetParameter(parameterName) {
		    var result = null,
		        tmp = [];
		    var items = location.search.substr(1).split("&");
		    for (var index = 0; index < items.length; index++) {
		        tmp = items[index].split("=");
		        if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
		    }
		    return result;
		}
	 
	 
	  function createRoute(startPatch,endPatch){
		    
	    	directionsDisplay.setMap(map);
	        	
	    	var startPos = new google.maps.LatLng(startPatch.coordinate.lat, startPatch.coordinate.lng);
	       	var endPos = new google.maps.LatLng(endPatch.coordinate.lat, endPatch.coordinate.lng);
	       	
	    	var patch2 = new google.maps.LatLng(28.616381, 77.231332);
	       	var patch3 = new google.maps.LatLng(28.614395, 77.234638);
	       	
	    	var wayPoints = [{
	            location: patch2,
	            stopover: false
	          },{
	              location: patch3,
	              stopover: true
	            }];   	
	    	
	    	
	    	var request = {
	    		      origin: startPos,
	    		      destination: endPos,
	    		      // Note that Javascript allows us to access the constant
	    		      // using square brackets and a string value as its
	    		      // "property."
	    		      travelMode: 'DRIVING',
	    		      waypoints: wayPoints
	    		  };
	    		  directionsService.route(request, function(response, status) {
	    		    if (status == 'OK') {
	    		      directionsDisplay.setDirections(response);
	    		    } else {
	    	            alert('Directions request failed due to ' + status);
	    	        }
	    		  });
	    		
	    }
   
	  function checkIfTankerReachedPatch(myLatLng){
		  
			$.each( patches,function(i, patch) {
				
				// Create an array of alphabetical characters used to label the markers.
				var curLatLng = new google.maps.LatLng(patch.coordinate.lat, patch.coordinate.lng);
		       	
				var circle  = new google.maps.Circle({
						strokeColor : '#0000FF',
						strokeOpacity : 0,
						strokeWeight : 0,
						fillColor : '#0000FF',
						fillOpacity : 0,
						map : map,
						center : curLatLng,
						radius : 25
				});
				
				if((google.maps.geometry.spherical.computeDistanceBetween(myLatLng, circle.center) <= circle.radius)){
					alert('Tanker has reached to the desired location'); 
					updatePatchStatus(patch);
	      		
	      		 }
			});
	  }
	  
	  function updatePatchStatus(patch){
		  
		  var mourl = "http://localhost:8080/AutomatedWateringSystem/rest/MonitoringDetails/updatePatchStatus";
	    	 $.ajax({
	    		 	url: mourl,
					dataType: "json",
					data: {
						 format: 'json',
					      'id': patch.id,
					},
					type: 'POST',
					success: function(data, textStatus, jqXHR){
						alert('Tanker has reached to the desired location'); 
					},
					error: function(jqXHR, textStatus, errorThrown){
						alert('error in updating patch status ' + textStatus);
					}
				   
				});
	  }
	  